/**
 * 
 */
package mesDonnées;

import java.util.Scanner;

/**
 * @author Rodolphe
 *
 */
public class ScannerUtil {
    public static Scanner scan;

    public static void ouvrir() {
        scan = new Scanner(System.in);
    }

    public static void fermer() {
        scan.close();
    }

    public static String saisir() {
        return scan.nextLine();
    }

}
