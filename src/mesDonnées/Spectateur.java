package mesDonn�es;

/**
 * @author Charline
 *
 */
public class Spectateur {

    private String nom;
    private String email;
    private int    age;

    /**
     * constructeur par d�faut
     */
    public Spectateur() {
        // empty method
    }

    /**
     * initialise un spectateur
     * 
     * @param nom
     * @param email
     * @param age
     * @return
     */
    public static Spectateur initSpectateur(final String nom, final String email, final int age) {
        Spectateur spectateur = new Spectateur();
        spectateur.setNom(nom);
        spectateur.setEmail(email);
        spectateur.setAge(age);
        return spectateur;
    }

    @Override
    public String toString() {
        return "Spectateur [nom=" + nom + ", email=" + email + ", age=" + age + "]";
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(final String nom) {
        this.nom = nom;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(final int age) {
        this.age = age;
    }

}
