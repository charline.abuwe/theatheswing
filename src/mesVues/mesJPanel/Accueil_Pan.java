/**
 * 
 */
package mesVues.mesJPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author 59013-07-04
 *
 */
public class Accueil_Pan extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = -1582432286212833839L;

    public Accueil_Pan() {
        //permet l'affichage dans une grille
        //au centre par defaut
        super(new GridBagLayout());
        
        this.setPreferredSize(new Dimension(700, 700));
        this.setBackground(Color.lightGray);
        this.setVisible(true);
        
        //On d�finit la police d'�criture � utiliser
        JLabel label = new JLabel("Bienvenue au Th�atre");
        Font police = new Font("Arial", Font.BOLD,70);
        label.setFont(police);

        this.add(label);

    }

}
