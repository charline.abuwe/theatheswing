/**
 * 
 */
package mesVues.mesActionListener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import mesDonnées.MethodesMenu;
import mesDonnées.Spectacle;
import mesDonnées.Spectateur;
import mesDonnées.Theatre;
import mesVues.maJFrame.Fenetre;
import mesVues.mesJPanel.AjoutSpectacle_Pan;

/**
 * @author 59013-07-04
 *
 */
public class ActionAjouterUnSpectacle implements ActionListener {

    Spectacle          spectacle = new Spectacle();
    AjoutSpectacle_Pan addPanel  = new AjoutSpectacle_Pan();

    /**
     * 
     */
    public ActionAjouterUnSpectacle(final Spectacle spectacle, AjoutSpectacle_Pan pan) {
        this.spectacle.setNumeroSpectacle(spectacle.getNumeroSpectacle());
        this.spectacle.setTitre(spectacle.getTitre());
        this.spectacle.setPrix(spectacle.getPrix());
        ArrayList<Spectateur> listeDesSpectateur = new ArrayList<Spectateur>();
        this.spectacle.setListesDesSpectateurs(listeDesSpectateur);
        addPanel.setVisible(true);
        addPanel = pan;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        addNewSpectacle(spectacle, Fenetre.theatre, addPanel);
        System.out.println(Fenetre.theatre.getListesDesSpectacles());

    }

    public static void addNewSpectacle(final Spectacle spectacle, final Theatre theatre, final AjoutSpectacle_Pan pan) {
        MethodesMenu.ajouterUnSpectacle(spectacle, Fenetre.theatre, pan);

    }

}
