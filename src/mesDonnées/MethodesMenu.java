/**
 * 
 */
package mesDonn�es;

import java.util.Iterator;

import javax.swing.JOptionPane;

import mesVues.mesJPanel.AjoutSpectacle_Pan;

/**
 * @author Charline contient la presentation de tous les menus pr�vus
 *
 */
public class MethodesMenu {

    /**
     * choix 1 :verifie si le numeroSpectacle saisi existe d�j�
     * 
     * @param spectacle
     * @param theatre
     * @return
     */
    public static boolean isNotExist(final Spectacle spectacle) {
        for (Spectacle spectacle1 : Theatre.getListesDesSpectacles()) {
            if (spectacle.getNumeroSpectacle() == spectacle1.getNumeroSpectacle()) {
                return false;
            }
        }
        return true;
    }

    /**
     * choix 1 :met � jour le numeroSpectacle si il existe d�j�
     * 
     * @param spectacle
     * @return
     */
    @SuppressWarnings("static-access")
    public static Spectacle changerNumero(final Spectacle spectacle, final Theatre theatre, final AjoutSpectacle_Pan pan) {
        JOptionPane erreurDeSaisie = new JOptionPane(), correction = new JOptionPane();
        String numero = String.valueOf(spectacle.getNumeroSpectacle());
        //verifie si le numero existe d�j�
        while (!isNotExist(spectacle)) {
            //tant que le numero existe il affiche la boite de dialogue pour recuperer un autre numero
            numero = erreurDeSaisie.showInputDialog(null, "Numero d�j� existant, Donner un autre numero ", "ErreurDeSaisie", JOptionPane.QUESTION_MESSAGE);
            spectacle.setNumeroSpectacle(Integer.valueOf(numero));
        }
        correction.showMessageDialog(null, "le numero saisi est  " + numero, "Correction", JOptionPane.INFORMATION_MESSAGE);
        //erreurDeSaisie.showMessageDialog(pan, "ERREUR : Le numero du Spectacle existe d�j�, donner un autre numero : ","Erreur de Saisie",JOptionPane.ERROR_MESSAGE);
        pan.getNumeroSpectacle().setText(numero);
        return spectacle;
    }

    /**
     * choix 1 :message OK Ajout d'un spectacle
     * 
     * @param spectacle
     * @param theatre
     */
    public static void messageOkAjout(final Spectacle spectacle, final Theatre theatre, final AjoutSpectacle_Pan pan) {
        JOptionPane messageOk = new JOptionPane();
        String message = "\n\nLe spectacle < " + spectacle.getTitre() + " > est ajout� \n\tPrix d'une place :\t\t< " + spectacle.getPrix() + " >";
        messageOk.showMessageDialog(pan, message);

        pan.setVisible(false);

    }

    /**
     * choix 1 :Valider ajouter un nouveau spectacle dans un theatre
     * 
     * @param spectacle
     * @param theatre
     * @return
     */
    public static void ajouterUnSpectacle(final Spectacle spectacle, final Theatre theatre, final AjoutSpectacle_Pan pan) {

        //verifie si le spectacle existe deja
        if (isNotExist(spectacle)) {
            //il n'exite pas ---> ajout
            Theatre.getListesDesSpectacles().add(spectacle);
        } else {
            //il existe d�ja ---> mise � jour numero
            pan.getNumeroSpectacle().setText("");
            Theatre.getListesDesSpectacles().add(changerNumero(spectacle, theatre, pan));
        }
        //affichage message ok ajout et menu
        MethodesMenu.messageOkAjout(spectacle, theatre, pan);

    }

    /**
     * choix 2 :verifie si le spectateur existe deja dans la liste de tous les spectateurs
     * 
     * @param email
     * @param theatre
     * @return
     */
    public static Spectateur validerSpectateur(final String email, final Theatre theatre) {
        //creation d'un iterator pour parcourir la hashSet de tousLesSpectateurs
        Iterator<Spectateur> tousLesSpectateurs = Theatre.getListesDeTousLesSpectateurs().iterator();

        //verifier si le spectateur existe deja dans le theatre
        while (tousLesSpectateurs.hasNext()) {
            Spectateur spectateur = (Spectateur) tousLesSpectateurs.next();
            if (spectateur.getEmail().equals(email)) {
                return spectateur;
            }
        }
        return null;
    }

    /**
     * choix 2 : initialise un nouveau spectateur quand il n'existe pas
     * 
     * @param theatre
     * @param email
     * @param spectateur
     */
    private static Spectateur initNouveauSpectateur(final String email) {
        System.out.println("Nom de la reservation ?");
        String nom = ScannerUtil.saisir();
        System.out.println("Age ?");
        int age = Integer.valueOf(ScannerUtil.saisir());
        Spectateur spectateur = new Spectateur();
        spectateur.setNom(nom);
        spectateur.setAge(age);
        spectateur.setEmail(email);

        return spectateur;
    }

    /**
     * consulte un spectacle demand�
     * 
     * @param theatre
     * @param numeroSpectacle
     * @return
     */
    private static Spectacle consulterSpectacleDemande(final int numeroSpectacle) {
        Spectacle spectacleDemande = Theatre.findspectacle(numeroSpectacle);
        //controle numero spectacle
        while (spectacleDemande == null) {
            System.err.println("Le numero n'est pas valide, choisissez un numero dans la liste");
            for (Spectacle spectacle : Theatre.getListesDesSpectacles()) {
                System.out.println("\t" + spectacle.getNumeroSpectacle() + " - " + spectacle.getTitre() + "\n");
            }
            spectacleDemande = Theatre.findspectacle(numeroSpectacle);
        }
        return spectacleDemande;
    }

    /**
     * choix 2 : consulter un spectateur dans la liste des spectateurs
     * 
     * @param theatre
     * @param email
     * @return
     */
    private static Spectateur consulterSpectateur(final Theatre theatre, String email) {
        //controle spectateur
        Spectateur spectateur = validerSpectateur(email, theatre);
        if (spectateur == null) {
            //si spectateur n'existe pas
            spectateur = initNouveauSpectateur(email);
            //enregister nouveau spectateur dans la liste de tous les spectateurs
            Theatre.getListesDeTousLesSpectateurs().add(spectateur);
        }
        return spectateur;
    }

    /**
     * choix 2 : affiche les infos de la vente
     * 
     * @param spectacleDemande
     * @param spectateur
     */
    private static void afficheInfoVente(Spectacle spectacleDemande, Spectateur spectateur) {
        System.out.println("Nom de la reservation : \n" + spectateur.getNom());
        System.out.println("Age : \n" + spectateur.getAge());
        //calculerPrix par rapport � l'age
        System.out.println("Votre tarif : \n" + spectacleDemande.calculPrixSpectacle(spectateur.getAge(), spectacleDemande));
    }

    /**
     * valider vente et message ok
     * 
     * @param theatre
     * @param spectacleDemande
     * @param spectateurEnCours
     * @param reponse
     * @return
     */
    private static void validerVente(final Theatre theatre, final Spectacle spectacleDemande, final Spectateur spectateurEnCours, final String reponse) {
        System.out.println("----------------------------------------------------------------");
        System.out.println("Bienvenue au Th�atre " + theatre.getNomDeTheatre());
        System.out.println("Accueil >---Vendre une Place\n");
        System.out.println("\tPLACE VENDUE");

        if (reponse.contentEquals("E")) {
            //ajouter le spectateur � la liste des spectateurs du spectacle demand�
            spectacleDemande.getListesDesSpectateurs().add(spectateurEnCours);
            //message ok
            System.out.println("Une place est r�serv�e � \t<Mr/Mme " + spectateurEnCours.getNom() + "> \npour la s�ance \t\t\t<" + spectacleDemande.getTitre() + ">\n");
            //incrementation recette
            double recette = Theatre.getRecette();
            recette = recette + spectacleDemande.getPrix();
            Theatre.setRecette(recette);
        } else {
            System.out.println("Operation Annul�e !\n");
        }
        System.out.println("(R)etour au menu ?");
        String choix = ScannerUtil.saisir();
        while (!choix.equals("R")) {
            System.err.println("ERREUR : tapper R pour retourner au menu !");
            choix = ScannerUtil.saisir();
        }
    }

    /**
     * Choix 2 : vendre une place
     * 
     * @param theatre
     */
    public static void vendreUnePlace(final Theatre theatre) {
        System.out.println("----------------------------------------------------------------");
        System.out.println("Bienvenue au Th�atre " + theatre.getNomDeTheatre());
        System.out.println("Accueil >---Vendre une Place");
        System.out.println("\tVENDRE UNE PLACE\n");
        System.out.println("\t--->Liste des Spectacles<---\n");

        //Liste des spectacles
        for (int i = 0; i < Theatre.getListesDesSpectacles().size(); i++) {
            System.out.println(i + 1 + " - " + Theatre.getListesDesSpectacles().get(i).getTitre());
        }

        //demander le numero du spectacle � voir
        // int numeroSpectacle = faireChoix();

        //consulte le spectacle demand�
        // Spectacle spectacleDemande = consulterSpectacleDemande(numeroSpectacle);

        //demander email spectateur
        System.out.print("\nVotre email svp?");
        String email = ScannerUtil.saisir();

        //consulter spectateur
        Spectateur spectateur = consulterSpectateur(theatre, email);

        //affichage infos vente du spectacle demand� par rapport au spectateur en cours
        //       afficheInfoVente(spectacleDemande, spectateur);

        //valider vente
        System.out.println("\n(E)nregistrer / (A)nnuler");
        String reponse = ScannerUtil.saisir();
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        while (!reponse.equals("E") && !reponse.equals("A")) {
            System.err.println("ERREUR : tapper E pour Enregistrer et A pour Annuler");
            reponse = ScannerUtil.saisir();
        }
        //valider la vente
        //      validerVente(theatre, spectacleDemande, spectateur, reponse);
    }

    //    /**
    //     * choix 3 : liste des spectacles
    //     * 
    //     * @param theatre
    //     * @return numero du spectacle choisi
    //     */
    //    public static int listeDesSpectacle(final Theatre theatre) {
    //        System.out.println("----------------------------------------------------------------");
    //        System.out.println("Bienvenue au Th�atre " + theatre.getNomDeTheatre());
    //        System.out.println("Accueil >--- Liste des Spectacles");
    //        System.out.println("\tLISTE DES SPECTACLES");
    //
    //        System.out.println("Indiquer le numero du spectacle pour avoir des infos (0 pour sortir) : ");
    //        //affichage liste des spectacles
    //        for (Spectacle spectacle : Theatre.getListesDesSpectacles()) {
    //            System.out.println(spectacle.getNumeroSpectacle() + " - " + spectacle.getTitre() + "\n");
    //        }
    //
    //        //numero du spectacle choisi
    // //       int numero = faireChoix();
    //        System.out.println("----------------------------------------------------------------");
    //
    //        //retourne le numero du spectacle choisi
    //        return numero;
    //    }

    /**
     * choix 3 :sous-menu infos spectacles
     * 
     * @param numeroSpectacle
     * @param theatre
     * @return
     */
    public static String infosSpectacle(final int numeroSpectacle, final Theatre theatre) {
        System.out.println("----------------------------------------------------------------");
        System.out.println("Bienvenue au Th�atre " + theatre.getNomDeTheatre());
        System.out.println("Accueil >---Liste des Spectacles >---Infos");
        System.out.println("\tINFO DU SPECTACLE");

        //recuperation du spectacle
        Spectacle spectacle = Theatre.findspectacle(numeroSpectacle);
        //affichage info spectacle
        System.out.println("\t" + spectacle.getTitre() + "\n\nPrix de la place(sans reduction)\t<" + spectacle.getPrix() + ">\nNombre de place(s) disponible(s)\t<"
                        + spectacle.nombreDePlacesDisponibles(spectacle.getListesDesSpectateurs()) + ">");
        //nombre de reservation
        System.out.println("Nombre de r�servation\t\t\t<" + spectacle.getListesDesSpectateurs().size() + ">\n");
        String reponse;
        do {
            System.out.println("(L)iste des spectateurs / (R)etour au menu");
            reponse = ScannerUtil.saisir();
        } while (!reponse.equals("L") && !reponse.equals("R"));

        return reponse;
    }

    /**
     * choix 3 : sous-menu Liste des spectateurs
     * 
     * @param theatre
     * @param spectacle
     */
    public static void listeDesSpectateursDuSpectacle(final Theatre theatre, final Spectacle spectacle) {
        System.out.println("----------------------------------------------------------------");
        System.out.println("Bienvenue au Th�atre " + theatre.getNomDeTheatre());
        System.out.println("Accueil >---Liste des Spectacles >---Infos >---Liste des Spectateurs");
        System.out.println("\tLISTE DES SPECTATEURS DU SPECTACLE \n\t--->" + spectacle.getTitre() + "<---\n");

        for (int i = 0; i < spectacle.getListesDesSpectateurs().size(); i++) {
            System.out.println(i + 1 + " - " + spectacle.getListesDesSpectateurs().get(i).getNom() + "\t" + spectacle.getListesDesSpectateurs().get(i).getAge() + "ans");
        }

        System.out.println("Retour au menu (0 pour sortir): ");
        int choix = Integer.valueOf(ScannerUtil.saisir());
        while (choix != 0) {
            System.err.println("ERREUR : tappez 0 pour sortir");
            choix = Integer.valueOf(ScannerUtil.saisir());
        }

    }

    /**
     * choix 4 : calcul la recette de tous les spectacles du theatre
     * 
     * @param theatre
     * @return
     */
    public static void recapitulerRecette(final Theatre theatre) {
        System.out.println("----------------------------------------------------------------");
        System.out.println("Bienvenue au Th�atre " + theatre.getNomDeTheatre());
        System.out.println("Accueil >---Recapitulatif Recette");
        System.out.println("\tRECETTE DU THEATRE\n");
        System.out.println("La recette est de " + Theatre.getRecette() + "� .\n");
        System.out.println("(R)etour au menu ?");
        String reponse = ScannerUtil.saisir();
        while (!reponse.equals("R")) {
            System.err.println("ERREUR : Tappez R pour retouner au menu");
            reponse = ScannerUtil.saisir();
        }
    }

    /**
     * choix 5 : valider Envoi Mail
     * 
     * @param rep
     */
    public static void validerSendMail(final String rep, final Theatre theatre) {
        System.out.println("----------------------------------------------------------------");
        System.out.println("Bienvenue au Th�atre " + theatre.getNomDeTheatre());
        System.out.println("Accueil >---Envoyer emails >---Validation Envoi");
        System.out.println("\n\tVALIDATION ENVOI\n");
        if (rep.equals("O")) {
            System.out.println("\nLes mails de promos sont envoy�s au spectateurs !");
        } else {
            System.out.println("\nMails non envoy�s ! ");
        }
    }

    /**
     * choix 5 : permet d'envoyer un mail � tous les spectateurs enregistr�s dans le theatre
     * 
     * @param theatre
     */
    public static void envoyerMailing(final Theatre theatre) {
        System.out.println("----------------------------------------------------------------");
        System.out.println("Bienvenue au Th�atre " + theatre.getNomDeTheatre());
        System.out.println("Accueil >---Envoyer email des promos au Spectateurs");
        System.out.println("\n\tENVOI MAIL PROMO AU SPECTATEUR\n");

        //afficher listes des mails des spectateurs
        System.out.println("\tListe des email :");
        int cpt = 1;
        for (Spectateur spectateur : Theatre.getListesDeTousLesSpectateurs()) {
            System.out.println(cpt + " - " + spectateur.getEmail() + " ;");
            cpt += 1;
        }

        System.out.print("\n\tEnvoyer emails (O)ui / (N)on\t:");
        String rep = ScannerUtil.saisir();
        while (!rep.equals("O") && !rep.equals("N")) {
            System.err.println("\nERREUR : Tappez O pour Oui ou N pour Non");
            rep = ScannerUtil.saisir();
        }
        //TODO method envoi mail + accuse de reception
        validerSendMail(rep, theatre);

        System.out.println("\n(R)etour au menu ?");
        String reponse = ScannerUtil.saisir();
        while (!reponse.equals("R")) {
            System.err.println("ERREUR : Tappez R pour retouner au menu");
            reponse = ScannerUtil.saisir();
        }
    }

}
