/**
 * 
 */
package mesVues.mesActionListener;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import mesDonnées.Spectacle;
import mesDonnées.Theatre;
import mesVues.maJFrame.Fenetre;
import mesVues.maJFrame.MaFenetreMenu;
import mesVues.mesJPanel.AjoutSpectacle_Pan;
import mesVues.mesJPanel.InfoSpectaclePan;
import mesVues.mesJPanel.ListeDesSpectaclesPan;

/**
 * @author 59013-07-04
 *
 */
public class LienMenuAction implements ActionListener {

    private JFrame fen;

    /**
     * 
     */
    public LienMenuAction() {
        
        // TODO Auto-generated constructor stub
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String item = ((JMenuItem) e.getSource()).getText().toString();
        fen = new MaFenetreMenu();
      //SORTIR
        if (item.equals("SORTIR")) {
            
            exit(fen);
        }
        //AJOUTER UN NOUVEAU SPECTACLE
        if (item.equals("AJOUTER UN NOUVEAU SPECTACLE")) {
            ajouterNewSpectacle(fen);
        }
        //LISTE DES SPECTACLES
        if (item.equals("LISTE DES SPECTACLES")) {
            afficherListeDesSpesctacles();
        }
        //INFOS SPECTACLE
        if (item.equals("INFOS SPECTACLE")) {
            afficherInfosSpectacle();
        }
    }
    
    /**
     * exit
     */
    private void exit(JFrame fen) {
        fen.dispose();
    }

    /**
     * affiche le pan de ajouter un spectacle
     */
    private void ajouterNewSpectacle(JFrame fen) {
        AjoutSpectacle_Pan pan = new AjoutSpectacle_Pan();
        fen.setContentPane(pan);
        fen.validate();
        fen.repaint();
    }

    /**
     * affiche le pan de la liste des spectacles
     */
    private void afficherListeDesSpesctacles() {
        ListeDesSpectaclesPan pan = new ListeDesSpectaclesPan();
        fen.setContentPane(pan);
        fen.validate();
        fen.repaint();
    }

    /**
     * affiche le pan Infos des Spectacles
     */
    private void afficherInfosSpectacle() {
        JOptionPane boiteAffichage = new JOptionPane(), confirmation = new JOptionPane();
        boiteAffichage.setPreferredSize(new Dimension(600, 150));
        
        ArrayList<Spectacle> liste = new ArrayList<Spectacle>();
        for (Spectacle spectacle : Theatre.getListesDesSpectacles()) {
            liste.add(spectacle);
        }
        
        Object[]  options = liste.toArray();
        Object value = boiteAffichage.showInputDialog(null, "Pour quelle spectacle voulez-vous des informations ?", "Liste des Spectacles", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        int index = liste.indexOf(value);
        confirmation.showMessageDialog(null, "Les Informations du Spectacles sont :\n " + liste.get(index).toString(), "Infos Spectacle", JOptionPane.INFORMATION_MESSAGE);
        
        InfoSpectaclePan pan = new InfoSpectaclePan(options,value);
        fen.setContentPane(pan);
        fen.validate();
        fen.repaint();
    }

}
