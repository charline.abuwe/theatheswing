/**
 * 
 */
package mesVues.maJFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.WindowConstants;

/**
 * @author 59013-07-04
 *
 */
public class MaFenetreMenu extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 8211972403265085953L;

   
    /**
     * 
     */
    public MaFenetreMenu() {
        super();
        //construction de la frame
        this.setTitle("THEATRE DES MILLE COLLINES");
        this.setSize(700, 600);
        this.setBackground(Color.black);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        
       
        //on rend la frame visible
        this.setVisible(true);
    }

}
