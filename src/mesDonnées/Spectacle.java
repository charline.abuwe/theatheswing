package mesDonn�es;

import java.util.ArrayList;

/**
 * @author Charline
 *
 */
public class Spectacle {

    private int                   numeroSpectacle;
    private String                titre;
    private double                prix;
    private ArrayList<Spectateur> listesDesSpectateurs = new ArrayList<Spectateur>();
    private final int             nombreDePlacesMaximum               = 99;

    /**
     * Constructeur par defaut
     */
    public Spectacle() {
        // empty method
    }

    /**
     * initialise un spectacle
     * 
     * @param numeroSpectacle
     * @param titre
     * @param prix
     * @return
     */
    public static Spectacle initSpectacle(final int numeroSpectacle, final String titre, final double prix, final ArrayList<Spectateur> listeDesSpectateurs) {
        Spectacle spectacle = new Spectacle();

        spectacle.setNumeroSpectacle(numeroSpectacle);
        spectacle.setTitre(titre);
        spectacle.setPrix(prix);
        spectacle.setListesDesSpectateurs(listeDesSpectateurs);

        return spectacle;
    }
    
    /**
     * calcul le nombre de place disponible
     * 
     * @param nbreDePlaceMax
     * @param listeDesSpectateursDuSpectacle
     * @return
     */
    public int nombreDePlacesDisponibles(final ArrayList<Spectateur> listeDesSpectateursDuSpectacle) {
        final int nombreDePlaceDisponible=nombreDePlacesMaximum-listeDesSpectateursDuSpectacle.size();
        if (nombreDePlaceDisponible > 0) {
            return nombreDePlaceDisponible;
        }
        return 0;
    }

    /**
     * calcul le prix en fonction de l'age
     * 
     * @param age
     * @return
     */
    public double calculPrixSpectacle(final int age, final Spectacle spectacle) {
        if (age >= 1 && age < 14) {
            this.prix = prix / 2;
        }
        return this.prix;
    }

    @Override
    public String toString() {
        return "Le n� de Spectacle est: " + (this.numeroSpectacle) + "\nLe titre est: " + this.titre + "\nLe prix est: " + this.prix + "\n\n\n";
    }

    /**
     * @return the numeroSpectacle
     */
    public int getNumeroSpectacle() {
        return numeroSpectacle;
    }

    /**
     * @param numeroSpectacle the numeroSpectacle to set
     */
    public void setNumeroSpectacle(final int numeroSpectacle) {
        this.numeroSpectacle = numeroSpectacle;
    }

    /**
     * @return the titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * @param titre the titre to set
     */
    public void setTitre(final String titre) {
        this.titre = titre;
    }

    /**
     * @return the prix
     */
    public double getPrix() {
        return prix;
    }

    /**
     * @param prix the prix to set
     */
    public void setPrix(final double prix) {
        this.prix = prix;
    }

    /**
     * @return the listesDesSpectateurs
     */
    public ArrayList<Spectateur> getListesDesSpectateurs() {
        return listesDesSpectateurs;
    }

    /**
     * @param listesDesSpectateurs the listesDesSpectateurs to set
     */
    public void setListesDesSpectateurs(final ArrayList<Spectateur> listesDesSpectateurs) {
        this.listesDesSpectateurs = listesDesSpectateurs;
    }

    /**
     * @return the nombreDePlacesMaximum
     */
    public int getNombreDePlacesMaximum() {
        return nombreDePlacesMaximum;
    }
    
}
