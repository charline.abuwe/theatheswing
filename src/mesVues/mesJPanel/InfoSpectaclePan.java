/**
 * 
 */
package mesVues.mesJPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import mesDonn�es.Spectacle;
import mesDonn�es.Theatre;

/**
 * @author 59013-07-04
 *
 */
public class InfoSpectaclePan extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 8528681039414246334L;

    Spectacle                 spectacle        = new Spectacle();

    /**
     * constructeur par defaut
     */
    public InfoSpectaclePan(Object[] options,Object value ) {
        //permet l'affichage dans une grille
        super();

        this.setPreferredSize(new Dimension(600, 500));
        this.setBackground(Color.lightGray);
        this.setLayout(new BorderLayout());
        this.setVisible(true);

        //-------------Titre panel
        //On d�finit la police d'�criture � utiliser
        JLabel labelTitre = new JLabel("INFOS SPECTACLE ");
        labelTitre.setBounds(100, 100, 10, 20);
        Font police = new Font("Calibri", Font.BOLD, 30);
        labelTitre.setFont(police);
        JPanel titrePan = new JPanel();
        titrePan.setPreferredSize(new Dimension(600, 50));
        titrePan.setBackground(Color.LIGHT_GRAY);
        titrePan.add(labelTitre);
        this.add(titrePan, BorderLayout.NORTH);

        //-------------Affichage
        //creation du panel des champs d'affichage
        JOptionPane boiteAffichage = new JOptionPane(), confirmation = new JOptionPane();
        boiteAffichage.setPreferredSize(new Dimension(600, 150));

        //les Champs d'affichage
        //----police des champs d'affichage
        Font police2 = new Font("Century Gothic", Font.ITALIC, 15);
        boiteAffichage.setFont(police2);
        confirmation.setFont(police2);

        //creation de la liste des spectacles dans le pannel
//        ArrayList<Spectacle> liste = new ArrayList<Spectacle>();
//        for (Spectacle spectacle : Theatre.getListesDesSpectacles()) {
//            liste.add(spectacle);
//        }
//       options = liste.toArray();
//       value = boiteAffichage.showInputDialog(null, "Pour quelle spectacle voulez-vous des informations ?", "Liste des Spectacles", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
//        int index = liste.indexOf(value);
//        confirmation.showMessageDialog(null, "Les Informations du Spectacles sont :\n " + liste.get(index).toString(), "Infos Spectacle", JOptionPane.INFORMATION_MESSAGE);

        //ajout dans le panel principal
//        this.add(boiteAffichage);
//        this.add(confirmation);
    }

}
