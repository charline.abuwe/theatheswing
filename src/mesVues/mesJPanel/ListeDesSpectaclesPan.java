/**
 * 
 */
package mesVues.mesJPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author 59013-07-04
 *
 */
public class ListeDesSpectaclesPan extends JPanel implements ActionListener {

    /**
     * 
     */
    private static final long serialVersionUID = 3270078482239839384L;
    
    public ListeDesSpectaclesPan() {
        //permet l'affichaeg dans une grille
        super();

        this.setPreferredSize(new Dimension(700, 700));
        this.setBackground(Color.lightGray);
        this.setVisible(true);

        //On d�finit la police d'�criture � utiliser
        JLabel labelTitre = new JLabel("LISTE DES SPECTACLES");
        Font police = new Font("Arial", Font.BOLD, 70);
        labelTitre.setFont(police);
        this.add(labelTitre);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

}
