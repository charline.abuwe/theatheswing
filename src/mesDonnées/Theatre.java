package mesDonn�es;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * @author Charline
 *
 */
public class Theatre {

    private String                      nomDeTheatre;
    private static ArrayList<Spectacle> listesDesSpectacles        = new ArrayList<Spectacle>();
    private static HashSet<Spectateur>  listesDeTousLesSpectateurs = new HashSet<Spectateur>();
    private static double               recette                    = 0;

    /**
     * constructeur par defaut
     */
    public Theatre() {
        // empty method
    }

    /**
     * initialise un theatre
     */
    public static Theatre initialiserTheatre() {
        Theatre lesMilleCollines = new Theatre();
        lesMilleCollines.setNomDeTheatre("THEATRE DES MILLE COLLINES");
        // On a fait des spectacles
        Spectacle spect1 = new Spectacle();
        spect1.setNumeroSpectacle(1);
        spect1.setTitre("Le No�l Fantastique");
        spect1.setPrix(232.3);
        Spectacle spect2 = new Spectacle();
        spect2.setNumeroSpectacle(2);
        spect2.setTitre("Les Aventures de Tintin");
        spect2.setPrix(180.50);
        Spectacle spect3 = new Spectacle();
        spect3.setNumeroSpectacle(3);
        spect3.setTitre("Java et ses Packages");
        spect3.setPrix(80.7);
        // On cr�e une liste de spectacle
        Theatre.getListesDesSpectacles().add(spect1);
        Theatre.getListesDesSpectacles().add(spect2);
        Theatre.getListesDesSpectacles().add(spect3);
        // On cr�e des spectateurs
        Spectateur spec1 = new Spectateur();
        spec1.setNom("Dupont");
        spec1.setAge(12);
        spec1.setEmail("dupont@yahoo.fr");
        Theatre.getListesDeTousLesSpectateurs().add(spec1);
        Spectateur spec2 = new Spectateur();
        spec2.setNom("DeVillier");
        spec2.setAge(16);
        spec2.setEmail("deVillier@yahoo.fr");
        Theatre.getListesDeTousLesSpectateurs().add(spec2);
        Spectateur spec3 = new Spectateur();
        spec3.setNom("Mousquetaire");
        spec3.setAge(25);
        spec3.setEmail("mousquetaire@yahoo.fr");
        Theatre.getListesDeTousLesSpectateurs().add(spec3);
        return lesMilleCollines;
    }

    /**
     * permet de recuperer un spectacle dans la liste des spectacles du theatre
     * 
     * @param numeroSpectacle
     * @return
     */
    public static Spectacle findspectacle(final int numeroSpectacle) {
        for (Spectacle spectacle : listesDesSpectacles) {
            if (numeroSpectacle == spectacle.getNumeroSpectacle()) {
                return spectacle;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Theatre [nomDeTheatre=" + nomDeTheatre + "\n listesDesSpectacles=" + listesDesSpectacles + "\n listesDeTousLesSpectateurs=" + listesDeTousLesSpectateurs + "]";
    }

    /**
     * @return the nomDeTheatre
     */
    public String getNomDeTheatre() {
        return nomDeTheatre;
    }

    /**
     * @param nomDeTheatre the nomDeTheatre to set
     */
    public void setNomDeTheatre(final String nomDeTheatre) {
        this.nomDeTheatre = nomDeTheatre;
    }

    /**
     * @return the listesDesSpectacles
     */
    public static ArrayList<Spectacle> getListesDesSpectacles() {
        return listesDesSpectacles;
    }

    /**
     * @param listesDesSpectacles the listesDesSpectacles to set
     */
    public static void setListesDesSpectacles(final ArrayList<Spectacle> listesDesSpectacles) {
        Theatre.listesDesSpectacles = listesDesSpectacles;
    }

    /**
     * @return the listesDeTousLesSpectateurs
     */
    public static HashSet<Spectateur> getListesDeTousLesSpectateurs() {
        return listesDeTousLesSpectateurs;
    }

    /**
     * @param listesDeTousLesSpectateurs the listesDeTousLesSpectateurs to set
     */
    public static void setListesDeTousLesSpectateurs(final HashSet<Spectateur> listesDeTousLesSpectateurs) {
        Theatre.listesDeTousLesSpectateurs = listesDeTousLesSpectateurs;
    }

    /**
     * @return the recette
     */
    public static double getRecette() {
        return recette;
    }

    /**
     * @param recette the recette to set
     */
    public static void setRecette(double recette) {
        Theatre.recette = recette;
    }

}
