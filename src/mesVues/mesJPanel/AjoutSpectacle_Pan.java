/**
 * 
 */
package mesVues.mesJPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import mesDonn�es.MethodesMenu;
import mesDonn�es.Spectacle;
import mesVues.maJFrame.Fenetre;
import mesVues.mesActionListener.ActionAjouterUnSpectacle;

/**
 * @author 59013-07-04
 *
 */
public class AjoutSpectacle_Pan extends JPanel implements ActionListener {

    /**
     * 
     */
    private static final long serialVersionUID = 3418308945561529479L;

    Spectacle                 spectacle        = new Spectacle();
    JTextField numeroSpectacle = new JTextField("00");
    JTextField titreSpectacle = new JTextField("Titre du Spectacle");
    JTextField prixSpectacle = new JTextField("22");
    JButton                   ajouter          = new JButton("AJOUTER");
    JButton                   annuler          = new JButton("ANNULER");

    /**
     * constructeur par defaut
     */
    public AjoutSpectacle_Pan() {
        //permet l'affichage dans une grille
        super();

        this.setPreferredSize(new Dimension(600, 500));
        this.setBackground(Color.lightGray);
        this.setLayout(new BorderLayout());
        this.setVisible(true);

        //-------------Titre panel
        //On d�finit la police d'�criture � utiliser
        JLabel labelTitre = new JLabel("AJOUTER UN NOUVEAU SPECTACLE ");
        labelTitre.setBounds(100, 100, 10, 20);
        Font police = new Font("Calibri", Font.BOLD, 30);
        labelTitre.setFont(police);
        JPanel titrePan = new JPanel();
        titrePan.setPreferredSize(new Dimension(600, 50));
        titrePan.setBackground(Color.LIGHT_GRAY);
        titrePan.add(labelTitre);
        this.add(titrePan, BorderLayout.NORTH);

        //----------------------
        //creation du panel des champs de saisie
        JPanel champDeTextePan = new JPanel();
        champDeTextePan.setPreferredSize(new Dimension(600, 150));

        //les Champs de saisie
        //----police des champs de saisie
        Font police2 = new Font("Comic Sans MS", Font.BOLD, 15);

        //-----champ numeroSpectacle
        numeroSpectacle.setPreferredSize(new Dimension(200, 20));
        //label nomant le champ de saisie
        JLabel label1 = new JLabel("Numero du Spectacle");
        label1.setFont(police2);
        label1.setPreferredSize(new Dimension(250, 20));
        //contruction de la ligne
        JPanel panLab = new JPanel();
        panLab.setPreferredSize(new Dimension(600, 35));
        panLab.add(label1);
        panLab.add(numeroSpectacle);
        //ajout dans le panel des champs de saisie
        champDeTextePan.add(panLab);

        //-----titreSpectacle
        
        titreSpectacle.setPreferredSize(new Dimension(200, 20));
        champDeTextePan.add(titreSpectacle);
        //label nomant le champ de saisie
        JLabel label2 = new JLabel("Titre du Spectacle");
        label2.setFont(police2);
        label2.setPreferredSize(new Dimension(250, 20));
        //contruction de la ligne
        JPanel panLab2 = new JPanel();
        panLab2.setPreferredSize(new Dimension(600, 35));
        panLab2.add(label2);
        panLab2.add(titreSpectacle);
        //ajout dans le panel des champs de saisie
        champDeTextePan.add(panLab2);

        //----prixSpectacle
        prixSpectacle.setPreferredSize(new Dimension(200, 20));
        champDeTextePan.add(prixSpectacle);
        //label nomant le champ de saisie
        JLabel label3 = new JLabel("Prix du Spectacle");
        label3.setFont(police2);
        label3.setPreferredSize(new Dimension(250, 20));
        //contruction de la ligne
        JPanel panLab3 = new JPanel();
        panLab3.setPreferredSize(new Dimension(600, 35));
        panLab3.add(label3);
        panLab3.add(prixSpectacle);
        //ajout dans le panel des champs de saisie
        champDeTextePan.add(panLab3);

        //ajout dans le panel principal
        this.add(champDeTextePan, BorderLayout.CENTER);

        //-----------------
        //creation du panel des bouton de validation
        JPanel boutonPan = new JPanel();
        boutonPan.setPreferredSize(new Dimension(200, 50));
        boutonPan.setBackground(Color.black);

        //creation des boutton
        
        ajouter.setSize(50, 20);
        ajouter.addActionListener(this);
        annuler.setSize(50, 20);
        annuler.addActionListener(this);
        

        boutonPan.add(ajouter);
        boutonPan.add(annuler);

        this.add(boutonPan, BorderLayout.SOUTH);


        //ajouter.addActionListener(new ActionAjouterUnSpectacle(spectacle, this));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        spectacle.setNumeroSpectacle(Integer.valueOf(numeroSpectacle.getText()));
        spectacle.setTitre(titreSpectacle.getText());
        spectacle.setPrix(Double.valueOf(prixSpectacle.getText()));

        String code = ((JButton) e.getSource()).getText();
        if (code.equals("AJOUTER")) {
            MethodesMenu.ajouterUnSpectacle(spectacle, Fenetre.theatre, this);
            System.out.println(Fenetre.theatre.getListesDesSpectacles());
        }
        if (code.equals("ANNULER")) {
            this.setVisible(false);
            System.out.println(Fenetre.theatre.getListesDesSpectacles());
        }
        
        
    }

    /**
     * @return the spectacle
     */
    public Spectacle getSpectacle() {
        return spectacle;
    }

    /**
     * @param spectacle the spectacle to set
     */
    public void setSpectacle(Spectacle spectacle) {
        this.spectacle = spectacle;
    }

    /**
     * @return the numeroSpectacle
     */
    public JTextField getNumeroSpectacle() {
        return numeroSpectacle;
    }

    /**
     * @param numeroSpectacle the numeroSpectacle to set
     */
    public void setNumeroSpectacle(JTextField numeroSpectacle) {
        this.numeroSpectacle = numeroSpectacle;
    }

    /**
     * @return the titreSpectacle
     */
    public JTextField getTitreSpectacle() {
        return titreSpectacle;
    }

    /**
     * @param titreSpectacle the titreSpectacle to set
     */
    public void setTitreSpectacle(JTextField titreSpectacle) {
        this.titreSpectacle = titreSpectacle;
    }

    /**
     * @return the prixSpectacle
     */
    public JTextField getPrixSpectacle() {
        return prixSpectacle;
    }

    /**
     * @param prixSpectacle the prixSpectacle to set
     */
    public void setPrixSpectacle(JTextField prixSpectacle) {
        this.prixSpectacle = prixSpectacle;
    }

    /**
     * @return the ajouter
     */
    public JButton getAjouter() {
        return ajouter;
    }

    /**
     * @param ajouter the ajouter to set
     */
    public void setAjouter(JButton ajouter) {
        this.ajouter = ajouter;
    }

    /**
     * @return the annuler
     */
    public JButton getAnnuler() {
        return annuler;
    }

    /**
     * @param annuler the annuler to set
     */
    public void setAnnuler(JButton annuler) {
        this.annuler = annuler;
    }
    
    

}



