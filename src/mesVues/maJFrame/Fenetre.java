/**
 * 
 */
package mesVues.maJFrame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import mesDonn�es.MethodesMenu;
import mesDonn�es.Theatre;
import mesVues.mesActionListener.ActionExit;
import mesVues.mesActionListener.LienMenuAction;
import mesVues.mesJPanel.AjoutSpectacle_Pan;

/**
 * @author 59013-07-04
 *
 */
public class Fenetre extends JFrame {
    
   public static  Theatre theatre = new Theatre();

    /**
     * 
     */
    private static final long serialVersionUID   = 2321790442418282540L;

    //Bar de menu
    private JMenuBar          barDeMenu          = new JMenuBar();

    //titre menu
    private JMenu             menu               = new JMenu("MENU");
    private JMenu             spectacle          = new JMenu("SPECTACLE");
    private JMenu             spectateur         = new JMenu("SPECTATEUR");
    private JMenu             envoieMail         = new JMenu("ENVOIE EMAIL");
    private JMenu             vente              = new JMenu("BILLET");

    //contenu menu
    private JMenuItem         ajoutSpectacle     = new JMenuItem("AJOUTER UN NOUVEAU SPECTACLE");
    private JMenuItem         listDesSpectacles  = new JMenuItem("LISTE DES SPECTACLES");
    private JMenuItem         infosSpectacles    = new JMenuItem("INFOS SPECTACLE");

    private JMenuItem         ajoutSpectateur    = new JMenuItem("AJOUTER UN NOUVEAU SPECTATEUR");
    private JMenuItem         listDesSpectateurs = new JMenuItem("LISTE DES SPECTATEURS");
    private JMenuItem         infosSpectateur    = new JMenuItem("INFOS SPECTATEUR");

    private JMenuItem         vendrePlace        = new JMenuItem("VENDRE UNE PLACE");
    private JMenuItem         recette            = new JMenuItem("RECAPITULATIF DE LA RECETTE");

    private JMenuItem         listeDesEmails     = new JMenuItem("LISTE DES EMAILS");

    private JMenuItem         sortir             = new JMenuItem("SORTIR");

    /**
     * constructeur qui initialise ma fenetre principale
     */
    public Fenetre() {
        super();
        //construction de la frame
        this.setSize(900, 700);
        this.setBackground(Color.lightGray);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);

        //message d'acceuil
        //permet l'affichage dans une grille
        //au centre par defaut
        this.setPreferredSize(new Dimension(700, 700));
        this.setBackground(Color.lightGray);
        this.setVisible(true);
        //On d�finit la police d'�criture � utiliser
        JLabel label = new JLabel("Bienvenue au Th�atre");
        Font police = new Font("Arial", Font.BOLD, 70);
        label.setFont(police);
        this.add(label);

        //On initialise nos menus 
        this.menu.add(sortir);

        this.spectacle.add(ajoutSpectacle);
        this.spectacle.add(listDesSpectacles);
        this.spectacle.add(infosSpectacles);

        this.spectateur.add(ajoutSpectateur);
        this.spectateur.add(listDesSpectateurs);
        this.spectateur.add(infosSpectateur);

        this.vente.add(vendrePlace);
        this.vente.add(recette);

        this.envoieMail.add(listeDesEmails);

        //ActionLister
        sortir.addActionListener(new ActionExit());
        ajoutSpectacle.addActionListener(new LienMenuAction());

        //on initialise la bar de menu
        this.barDeMenu.add(menu);
        this.barDeMenu.add(spectacle);
        this.barDeMenu.add(spectateur);
        this.barDeMenu.add(vente);
        this.barDeMenu.add(envoieMail);

        //je met la bar de Menu dans la frame
        this.setJMenuBar(barDeMenu);

        //je met le pannel d'accueil dans la frame principale
        //this.setContentPane(new Accueil_Pan());
        //on rend la frame visible
        this.setVisible(true);

    }

    /**
     * @param args
     */
    public static void main(final String[] args) {

        //creation de la fenetre
        Fenetre fenetre = new Fenetre();
        fenetre.theatre = Theatre.initialiserTheatre();
        fenetre.setTitle(fenetre.theatre.getNomDeTheatre());

    }

}
